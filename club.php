<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>

<div class="container-fluid">
    <?php
      // Requête pour récupérer les informations d'un club grâce à son id passé dans l'URL (method GET)
      $id = $_GET['id'];
      $sql = "SELECT * FROM clubs WHERE id = :id";
      $fields = ['id' => $id];
      $infosClub = Database::getInstance()->request($sql, $fields);
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Ligues
                </li>

                <li id="ligue_active" class="active">
                  <!-- Affiche le nom de la ligue -->
                  <?= $infosClub->ligue; ?>
                </li>

                <li class="active">
                    <i class="fa fa-edit"></i> Clubs
                </li>

                <li id="club_actif" class="active">
                  <!-- Affiche le nom du club -->
                  <?= $infosClub->nom_clubs; ?>
                </li>
            </ol>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-5 col-md-5 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-send fa-5x"></i>
                        </div>
                        <div id="status" name="status" class="col-xs-9">
                          <!-- Affiche les informations concernant la localisation du club -->
                          <div><strong>Adresse : </strong><?= $infosClub->adresse; ?></div>
                          <div><strong>Code postal : </strong><?= $infosClub->cp; ?></div>
                          <div><strong>Ville : </strong><?= $infosClub->ville; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-5">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9">
                          <div id="corresClub">
                            <!-- Affiche les informations concernant le correspondant du club -->
                            <div><strong>Correspondant : </strong><?= $infosClub->corres; ?></div>
                            <div><strong>Email : </strong><?= $infosClub->mail; ?></div>
                            <div><strong>Téléphone : </strong><?= $infosClub->tel; ?></div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="licensees" name="licensees" class="huge">
                              <!-- Affiche le nombre de licenciés du club -->
                              <?= $infosClub->licensees; ?>
                            </div>
                            <div>Licenciés</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="ajoutcontact">
      <center><a class="btn btn-primary" href="new_contact.php?id=<?= $infosClub->id; ?>">Créer un contact pour ce club</a></center>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h3 style="text-align:center;"> Contacts du club </h3>
        <div id="listecontact" name="listecontact" class="jumbotron">
          <?php
            // Requête permettant de récupérer la liste des contact pour un club dont l'id est passé dans l'URL (method GET)
            $id = $_GET['id'];
            $sql = "SELECT co.* FROM contact co, clubs c WHERE c.id = :id AND co.nom_clubs=c.nom_clubs";
            $fields = ['id' => $id];
            $listeContact = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeContact as $contact): ?>
            <center>
              <div class="card">
                <h3 class="card-header"><?= $contact->contact; ?></h3>
                <div class="card-block">
                  <p class="card-text"><?= $contact->mail; ?></p>
                  <p class="card-text"><?= $contact->tel; ?> </p>
                    <a href="modif_contact.php?id=<?= $contact->id; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a onclick="supprimerContact(<?= $contact->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                </div>
              </div>
            </center>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <br>
    <div id="compterendu">
      <center><a class="btn btn-primary" href="new_cr.php?id=<?= $infosClub->id; ?>">Créer un compte rendu pour ce club</a></center>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h3 style="text-align:center;"> Comptes rendus du club </h3>
        <div id="listecr" name="listecr" class="jumbotron">
          <?php
            // Requête permettant de récupéter les comptes rendus pour un club
            $id = $_GET['id'];
            $sql = "SELECT co.* FROM compterendu co, clubs c WHERE c.id = :id AND co.nom_clubs=c.nom_clubs";
            $fields = ['id' => $id];
            $listeCr = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeCr as $cr): ?>
            <div class="card">
              <h3 class="card-header"><?= $cr->nom_clubs; ?><small> <?= $cr->date; ?> </small></h3>
                <div class="card-block">
                  <h4 class="card-title"><?= $cr->raison; ?></h4>
                  <p style="font-size: 16px;" class="card-text"><?= $cr->corps; ?></p>
                  <a href="modif_cr.php?id=<?= $cr->id; ?>&club=<?= $cr->nom_clubs; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a onclick="supprimerCompterendu(<?= $cr->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                  <!-- Tester avec la fonction mail de php  -->
                  <!-- <a href="mailto:?subject=Compte Rendu du club : '+ nom_clubs_ +'&body=Date: Le '+ date_ +', %0D%0A%0D%0A Correspondant: '+corres_+'%0D%0A Mail: '+mail_+'%0D%0A Tel: 0'+tel_+' %0D%0A%0D%0A Motif: '+raison_+' %0D%0A%0D%0A Compte rendu: '+corps_+' " class="btn btn-info">Envoyer mail</a> -->
                </div>
            </div>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <center><a class="btn btn-info" href="https://drive.google.com/drive/u/2/folders/0B3Y_kdvi3603bUk3RFpzYkl1SVk">Drive</a></center>
    <br><br><br>

</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/liste_cr.js"></script>
<script src="js/liste_contact.js"></script>

</body>
</html>
