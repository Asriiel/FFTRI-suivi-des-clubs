<?php
session_start();
unset($_SESSION['auth']);
// Message pour informer l'utilisateur stocké dans $_SESSION['flash']
$_SESSION['flash']['success'] = " Vous êtes maintenant déconnecté";
// Redirection vers la page de login
header('Location: login.php');
?>
