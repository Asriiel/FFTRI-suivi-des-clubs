<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
  include_once 'Database.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Application au service des conseillers territoriaux pour la gestion des clubs">
    <meta name="author" content="Jessy Cauquy">
    <title>F.F.Tri</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon_fftri.png">
</head>

<body>

    <div id="wrapper">

      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <img src="logo.png" alt="fftri" width="100">
              <a class="navbar-brand" href="index.php">F.F.TRI</a>
          </div>

          <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                <!-- Si l'utilisateur est authentifié  -->
                <?php if (isset($_SESSION['auth'])): ?>

                  <li>
                    <a href="logout.php"><i class="fa fa-fw fa-sign-out"></i> Se déconnecter</a>
                  </li>
                  <li class="active">
                      <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                  </li>
                  <li>
                      <a href="liste_cr.php"><i class="fa fa-fw fa-bar-chart-o"></i> Liste des comptes rendus</a>
                  </li>
                  <li>
                      <a href="listeContact.php"><i class="fa fa-fw fa-users"></i> Liste des contacts</a>
                  </li>
                  <li>
                    <a href="admin.php"><i class="fa fa-fw fa-cog"></i> Administration </a>
                  </li>
                  <!-- sinon  -->
                <?php else: ?>

                  <li class="active">
                    <a href="login.php"><i class="fa fa-fw fa-sign-in"></i> Se connecter</a>
                  </li>

                <?php endif; ?>

              </ul>
          </div>
      </nav>
