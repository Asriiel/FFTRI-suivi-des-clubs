<?php
// Header obligatoire pour les requêtes AJAX
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
?>
<?php
include_once 'Database.php';

$contact = $_GET['contact'];
$mail = $_GET['mail'];
$tel = $_GET['tel'];
$club = $_GET['club'];

$sql = "SELECT * FROM contact WHERE mail = :mail";
$fields = ['mail' => $mail];
$instance = Database::getInstance()->request($sql, $fields);
if ($instance) {
  $sql = "DELETE FROM contact WHERE mail = :mail";
  $fields = ['mail' => $mail];
  $instance = Database::getInstance()->request($sql, $fields);
}

$sql = "INSERT INTO contact (nom_clubs, contact, tel, mail) VALUES (:nom_clubs, :contact, :tel, :mail)";
$fields = ['nom_clubs' => $club, 'contact' => $contact, 'tel' => $tel, 'mail' => $mail];
$user = Database::getInstance()->request($sql, $fields, false);
