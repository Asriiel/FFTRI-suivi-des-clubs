<?php
  // Démarre une session pour l'utilisateur
  session_start();
  // Fichier ou sont / seront stockés toute les fonctions PHP à réutiliser (ex: debug() )
  require_once 'function.php';
  // Indique que pour accéder à cette page l'utilisateur doit être connecté
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>

<div id="page-wrapper">

    <div class="container-fluid">
      <!-- Méthode permettant d'afficher les messages contenu dans $_SESSION['flash'] -->
      <?php if (isset($_SESSION['flash'])): ?>
        <?php foreach ($_SESSION['flash'] as $type => $message): ?>
          <?php if ($type == 'success'): ?>
          <div class="alert alert-<?= $type; ?>">
            <?= $message; ?>
          </div>
        <?php endif; ?>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
      <?php endif; ?>
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    F.F.TRI <small>Gestion des clubs</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> F.F.TRI
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Acceuil
                    </li>
                </ol>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-globe fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="nbligues" name="nbligues" class="huge">
                                  <?php
                                    // Requête permettant de récupérer le nombre de ligue
                                    $sql = "SELECT count(*) AS nb FROM ligues";
                                    $fields = [];
                                    $countLigues = Database::getInstance()->request($sql, $fields, false);
                                    echo $countLigues->nb;
                                  ?>
                                </div>
                                <div>Ligues</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-group fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="clubsAffiliés" name="clubsAffiliés" class="huge">
                                  <?php
                                    // Requête permettant de récupérer le nombre de club
                                    $sql = "SELECT count(*) AS nb FROM clubs";
                                    $fields = [];
                                    $countClubs = Database::getInstance()->request($sql, $fields, false);
                                    echo $countClubs->nb;
                                  ?>

                                </div>
                                <div>Clubs affiliés</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-user fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="licensees" name="licensees" class="huge">
                                  <?php
                                    /*
                                       Méthode permettant de récupérer toute les infos
                                       sur  les ligues pour afficher les liens vers
                                       les pages correspondantes
                                    */
                                    $sql = "SELECT * FROM ligues";
                                    $fields = [];
                                    $countLicensees = Database::getInstance()->request($sql, $fields, true);
                                    $cpt = 0;
                                  ?>

                                  <?php foreach ($countLicensees as $count): ?>
                                    <?php $cpt += $count->licensees; ?>
                                  <?php endforeach; ?>

                                  <?php echo $cpt; ?>
                                </div>
                                <div>Licenciés</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-globe fa-fw"></i> Liste des ligues</h3>
                    </div>
                    <div class="panel-body">
                        <div class="list-group">
                          <div id="status" name="status">
                            <?php
                              /*
                                 Méthode permettant de récupérer toute les infos
                                 sur  les ligues pour afficher les liens vers
                                 les pages correspondantes
                              */
                              $sql = "SELECT * FROM ligues ORDER BY nom_ligue ASC";
                              $fields = [];
                              $listeLigue = Database::getInstance()->request($sql, $fields, true);
                              $cpt = 0;

                            ?>
                            <!-- Pour chaque occurence de ligue -->
                            <?php foreach ($listeLigue as $ligue): ?>
                              <?php
                                /*
                                  On effectue une requête pour déterminer
                                  le nombre de club pour chaque ligue
                                */
                                $sql = "SELECT COUNT(*) AS nb FROM clubs WHERE ligue = :ligue";
                                $fields = ['ligue' => $ligue->nom_ligue];
                                $countClubsLigue = Database::getInstance()->request($sql, $fields);
                              ?>
                              <!-- On crée le lien correspondant à chaque ligue
                                   avec le nombre de clubs affiché à droite -->
                              <a href="ligue.php?id=<?= $ligue->id; ?>" class="list-group-item"><span class="badge"><?= $countClubsLigue->nb; ?></span><i class="fa fa-fw fa-users"></i> <?= $ligue->nom_ligue; ?></a>
                            <?php endforeach; ?>
                          </div>
                          <ul class="pagination" id="pagin">
                          </ul>
                            <br>
                            <div class="col-sm-3">
                              <label class="sr-only">Nom</label>
                              <input type="text" onkeyup="recherche(event)" id="nametxt" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Rechercher">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Import JS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- <script src="js/index.js"></script> -->

</body>
</html>
