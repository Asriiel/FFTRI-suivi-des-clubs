/** Prototype Object Clubs

1001 : Object
      address : "2 RUE HUBERT MOUCHEL"
      city : "LES MUREAUX"
      clubEmail : "freulon.guillaume@gmail.com"
      clubLicensees : "137"
      clubPhone : "01 30 99 74 52"
      contactEmail : null
      contactFirstName : "Guillaume"
      contactLastName : "FREULON"
      contactPhone : null
      correspondantEmail : "freulon.guillaume@gmail.com"
      correspondantFirstName : "Guillaume"
      correspondantLastName : "FREULON"
      correspondantPhone : "0614660754"
      id : "1001"
      leagueName : "Ile-de-France"
      name : "TRINOSAURE"
      website : "www.trinosaure.com"
      zip : "78130"

*/

$("#AjaxClubs").click(function(){

    $.ajax({
       url : 'http://api.espacetri.fftri.com/api/get-clubs-offline-app',
       type : 'GET',
       dataType : 'json',
       contentType : 'application/vnd.espacetri.v2+json',
       headers: {
         'accept': 'application/json',
         'Authorization': 'Basic ' + btoa('fftri:Z^f~6\\NF/L3gQVa!')
       },

       success : function(data){
        //  console.log(data);
         for (item in data) {
           var ligue = data[item]["leagueName"];
           var nom_clubs = data[item]["name"];
           var id = data[item]["id"];
           var cp = data[item]["zip"];
           var adresse = data[item]["address"];
           var ville = data[item]["city"];
           var corres = data[item]["correspondantFirstName"]+" "+data[item]["correspondantLastName"];
           var mail = data[item]["correspondantEmail"];
           var tel = data[item]["correspondantPhone"];
           var contact = data[item]["contactFirstName"]+" "+data[item]["contactLastName"];
           var mail_c = data[item]["contactEmail"];
           var tel_c = data[item]["contactPhone"];
           var licensees = data[item]["clubLicensees"];
           insertClub(ligue, nom_clubs, id, cp, adresse, ville, corres, mail, tel, licensees);
         }
       },

       error : function(resultat, statut, erreur){
         console.log("Une erreur est survenu. ");
       },
    });
});

// Appel Ajax à la méthode d'insertion en base pour les clubs
function insertClub(ligue, nom_clubs, id, cp, adresse, ville, corres, mail, tel, licensees){
  $.ajax({
       url : 'insert_Club.php',
       type : 'GET',
       data : 'ligue=' + ligue + '&nom_clubs=' + nom_clubs + '&id=' + id + '&cp=' + cp + '&adresse=' + adresse + '&ville=' + ville + '&corres=' + corres + '&tel=' + tel + '&mail=' + mail + '&licensees=' + licensees,
       success : function(data, statut){
         console.log("It wooooorks ! "+ statut);
       }
    });
}

/** Prototype Object Ligues

1 : Object
    leagueLicensees : "7743"
    leagueaddress : "MADAME KOUAHO  ESTELLE  2 RUE DU SENEGAL"
    leaguecity : "PARIS                           "
    leagueemail : "estelle.kouaho@idftriathlon.com"
    leagueid : "1"
    leaguename : "Ile-de-France"
    leaguephone : "0140339543"
    leaguewebsite : "www.idftriathlon.com"
    leaguezip : "75020"

*/

$("#AjaxLigues").click(function(){

    $.ajax({
       url : 'http://api.espacetri.fftri.com/api/get-leagues-offline-app',
       type : 'GET',
       dataType : 'json',
       contentType : 'application/vnd.espacetri.v2+json',
       headers: {
         'accept': 'application/json',
         'Authorization': 'Basic ' + btoa('fftri:Z^f~6\\NF/L3gQVa!')
       },
       success : function(data){

         for (item in data) {
           var id = data[item]["leagueid"];
           var nom_ligue = data[item]["leaguename"];
           var adresse = data[item]["leagueaddress"];
           var cp = data[item]["leaguezip"];
           var ville = data[item]["leaguecity"];
           var tel = data[item]["leaguephone"];
           var mail = data[item]["leagueemail"];
           var licensees = data[item]["leagueLicensees"];
           insertLigue(id, nom_ligue, adresse, cp, ville, tel, mail, licensees);

         }
       },
       error : function(resultat, statut, erreur){
         console.log("Une erreur est survenu avec le code suivant : "+erreur);
       },
    });
});

// Appel Ajax à la méthode d'insertion en base pour les ligues
function insertLigue(id, nom_ligue, adresse, cp, ville, tel, mail, licensees){
  $.ajax({
       url : 'insert_Ligue.php',
       type : 'GET',
       data : 'id=' + id + '&nom_ligue=' + nom_ligue + '&adresse=' + adresse + '&cp=' + cp + '&ville=' + ville + '&tel=' + tel + '&mail=' + mail + '&licensees=' + licensees,
       success : function(data, statut){
         console.log("It wooooorks ! "+ statut);
       }
    });
}



$("#AjaxGetContact").click(function(){
    $.ajax({
       url : 'fetch_contact.php',
       type : 'GET',
       dataType : 'json',
       contentType : 'application/json',
       success : function(data){
         for (var i = 0; i < data.length; i++) {
           console.log(data[i]["nom_clubs"]);
         }
       },
       error : function(resultat, statut, erreur){
         console.log("Erreur : "+erreur);
       },
    });
});

$("#AjaxAddContact").click(function(){
    // var contact = "Rinchou";
    // var mail = "Rinchou@gmail.com";
    // var tel = "0601311351";
    // var club = "Aura Kingdom 2";
    $.ajax({
       url : 'insert_contact.php',
       type : 'GET',
       data : 'contact=' + contact + '&mail=' + mail + '&tel=' + tel + '&club=' + club,
       success : function(data){
         console.log("ok");
       },
       error : function(resultat, statut, erreur){
         console.log("pas ok");
       },
    });
});
