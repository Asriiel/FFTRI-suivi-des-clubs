// Fonction permettant d'afficher la date actuel sous un format spécifique dans la page compterendu.php
$( document ).ready(function() {
  var date = new Date();
  document.getElementById("date").value = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+", "+(date.getHours()+1)+":"+(date.getMinutes())+":"+date.getSeconds();
});
