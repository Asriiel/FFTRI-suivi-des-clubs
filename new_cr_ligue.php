<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>
<!-- ~~~~~~ -->
<?php

if (!empty($_POST)) {
  $date = $_POST['date'];
  $nom_clubs = $_POST['nom_clubs'];
  $raison = $_POST['raison'];
  $corps = $_POST['corps'];
  // On test si la checkbox est coché ( Pour savoir qui utiliser entre le correspondant et le contact s'il existe)
  if (isset($_POST['check'])) {
    $contact = $_POST['contact'];
    // Si oui on lance une requête SQL pour déterminer le mail et le tel du contact
    $sql = "SELECT * FROM contact WHERE contact = :contact AND nom_clubs = :nom_clubs";
    $fields = ['contact' => $contact, 'nom_clubs' => $nom_clubs];
    $getcontact = Database::getInstance()->request($sql, $fields);
    // On ajoute les données dans les variables
    $mail = $getcontact->mail;
    $tel = $getcontact->tel;

  } else { // Sinon on utilise le correspondant prévu de base
    $contact = $_POST['selectCorres'];
    $mail = $_POST['mail'];
    $tel = $_POST['tel'];
  }
  // Requête SQL pour ajouter le compte rendu en base
  $sql = "INSERT INTO compterendu (nom_clubs, contact, tel, mail, raison, corps, date) VALUES (:nom_clubs, :contact, :tel, :mail, :raison, :corps, :date)";
  $fields = ['nom_clubs' => $nom_clubs, 'contact' => $contact, 'tel' => $tel, 'mail' => $mail, 'raison' => $raison, 'corps' => $corps, 'date' => $date];
  $user = Database::getInstance()->request($sql, $fields, false);
  // Message de notification pour l'utilisateur [success]
  $_SESSION['flash']['success'] = "Compte rendu ajouté avec succès !";
  // Redirection vers la page liste_cr.php pour afficher tout les compte rendus
  header('Location: liste_cr.php');
}
?>
<div class="container-fluid">
    <?php
      $id = $_GET['id'];
      $sql = "SELECT * FROM ligues WHERE id = :id";
      $fields = ['id' => $id];
      $getLigue = Database::getInstance()->request($sql, $fields);
    ?>

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Ligues
                </li>

                <li id="ligue_active" class="active">
                  <?= $getLigue->nom_ligue; ?>
                </li>


            </ol>
        </div>
    </div>
  <form action="" method="POST" role="form" class="form-horizontal">
      <fieldset>

      <!-- Form Name -->
      <legend>Créer un compte rendu</legend>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="date">Date</label>
        <div class="col-md-4">
        <input id="date" name="date" placeholder="placeholder" class="form-control input-md" required="" type="text">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="nom_clubs">Nom de la ligue</label>
        <div class="col-md-4">
        <input id="nom_clubs" name="nom_clubs" placeholder="placeholder" class="form-control input-md" required="true" value="<?= $getLigue->nom_ligue; ?>" type="text">

        </div>
      </div>

      <div class="jumbotron">
          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="corres">Correspondant ligue</label>
            <div id="select" name="select" class="col-md-4">
            <input id="selectCorres" name="selectCorres" placeholder="placeholder" class="form-control input-md" value="" type="text">
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="mail">Mail</label>
            <div class="col-md-4">
            <input id="mail" name="mail" placeholder="placeholder" class="form-control input-md" value="" type="text">

            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="tel">Téléphone</label>
            <div class="col-md-4">
            <input id="tel" name="tel" placeholder="placeholder" class="form-control input-md" value="" type="text">

            </div>
          </div>
      </div>
      <?php
        $id = $_GET['id'];
        $sql = "SELECT co.* FROM contact co, ligues l WHERE l.id = :id AND co.nom_clubs=l.nom_ligue";
        $fields = ['id' => $id];
        $getContact = Database::getInstance()->request($sql, $fields, true);
      ?>
      <?php if ($getContact): ?>
        <div id="havecontact" class="jumbotron">
          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="corres">Contact club</label>
            <div id="selectContact" name="selectContact" class="col-md-4">
              <select onchange="changeValueContact()" id="contact" name="contact" class="form-control">
                <option value=""></option>
                <?php foreach ($getContact as $contact): ?>
                  <option value="<?= $contact->contact; ?>"><?= $contact->contact; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

        </div>
        <?php else: ?>
          <div class="jumbotron">
            <center><p>Pas de contact pour ce club</p></center>
          </div>
        <?php endif; ?>

      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="raison">Objet</label>
        <div class="col-md-4">
          <select id="raison" name="raison" class="form-control">
            <option value="Projet associatif">Projet associatif</option>
            <option value="Fonctionnement associatif">Fonctionnement associatif</option>
            <option value="Création d'emploi">Création d'emploi</option>
            <option value="Suivi d'emploi">Suivi d'emploi</option>
            <option value="RGF / RTS">RGF / RTS</option>
            <option value="Santé">Santé</option>
            <option value="Paratri">Paratri</option>
            <option value="Mixité">Mixité</option>
            <option value="Développement durable">Développement durable</option>
            <option value="Citoyenneté">Citoyenneté</option>
            <option value="Autre">Autre</option>
          </select>
        </div>
      </div>

      <!-- Textarea -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="corps">Corps du compte rendu</label>
        <div class="col-md-4">
          <textarea rows="10" class="form-control" id="corps" required="true" name="corps"></textarea>
        </div>
      </div>
      <div class="checkbox">
        <center><label><input name="check" id="check" type="checkbox" value="">Utiliser le contact dans le compte rendu</label></center>
      </div><br>
      <!-- Button -->
      <div class="form-group">

        <div class="col-md-4 col-md-offset-4">
          <div class="col-sm-6">
            <button class="btn btn-default" type="submit" name="button">Enregistrer</button>
          </div>
          <div name="mailGen" id="mailGen" class="col-sm-6">

          </div>
        </div>
      </div>

      </fieldset>
    </form>
</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/compterendu.js"></script>

</body>
</html>
