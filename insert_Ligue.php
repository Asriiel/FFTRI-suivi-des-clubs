<?php
include_once 'Database.php';

// Récupération des paramètres dans l'URL
$id = $_GET['id'];
$nom_ligue = $_GET['nom_ligue'];
$adresse = $_GET['adresse'];
$cp = $_GET['cp'];
$ville = $_GET['ville'];
$tel = $_GET['tel'];
$mail = $_GET['mail'];
$licensees = $_GET['licensees'];

// Il est possible qu'avec l'API la valeur de licensees soit null, on la force donc à 0 pour éviter les problemes avec la BD
if ($licensees == 'undefined') {
  $licensees = 0;
}

// Requête pour insérer une ligue dans la BD
$sql = "INSERT INTO ligues (id, nom_ligue, adresse, cp, ville, tel, mail, licensees) VALUES (:id, :nom_ligue, :adresse, :cp, :ville, :tel, :mail, :licensees)";
$fields = [
           'id' => $id,
           'nom_ligue' => $nom_ligue,
           'adresse' => $adresse,
           'cp' => $cp,
           'ville' => $ville,
           'tel' => $tel,
           'mail' => $mail,
           'licensees' => $licensees
         ];

$user = Database::getInstance()->request($sql, $fields, false);
