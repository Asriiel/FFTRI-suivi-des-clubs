<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>
<div class="container-fluid">

  <?php if (isset($_SESSION['flash'])): ?>
    <?php foreach ($_SESSION['flash'] as $type => $message): ?>
      <div class="alert alert-<?= $type; ?>">
        <center><?= $message; ?></center>
      </div>
    <?php endforeach; ?>
    <?php unset($_SESSION['flash']); ?>

  <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Liste des contacts
                </li>

            </ol>
        </div>
    </div>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <h3 style="text-align:center;">Liste des contacts </h3>
        <div id="listecr" name="listecr" class="jumbotron">
          <?php
            $sql = "SELECT * FROM contact";
            $fields = [];
            $listeContact = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeContact as $contact): ?>
            <center>
              <div class="card">
                <h3 class="card-header"><?= $contact->contact; ?></h3>
                <div class="card-block">
                  <p class="card-text"><?= $contact->mail; ?></p>
                  <p class="card-text"><?= $contact->tel; ?></p>
                  <a href="modif_contact.php?id=<?= $contact->id; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a href="#" onclick="supprimerContact(<?= $contact->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                </div>
              </div>
            </center>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/liste_contact.js"></script>

</body>
</html>
