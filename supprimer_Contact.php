<?php
include_once 'Database.php';

$id = $_GET['id'];
$sql = "DELETE FROM contact WHERE id = :id";
$fields = ['id' => $id];

$user = Database::getInstance()->request($sql, $fields);
$_SESSION['flash']['success'] = "Contact supprimé avec succès !";
