<?php
include_once 'Database.php';

// Récupération des paramètres dans l'URL
$ligue = $_GET['ligue'];
$nom_clubs = $_GET['nom_clubs'];
$id = $_GET['id'];
$cp = $_GET['cp'];
$adresse = $_GET['adresse'];
$ville = $_GET['ville'];
$corres = $_GET['corres'];
$mail = $_GET['mail'];
$tel = $_GET['tel'];
$licensees = $_GET['licensees'];

// Il est possible qu'avec l'API la valeur de licensees soit null, on la force donc à 0 pour éviter les problemes avec la BD
if ($licensees == 'undefined') {
  $licensees = 0;
}
if ($cp == 'undefined') {
  $cp = 0;
}

// Requête pour insérer un club dans la BD
$sql = "INSERT INTO clubs (ligue, nom_clubs, id, cp, adresse, ville, corres, mail, tel, licensees) VALUES (:ligue, :nom_clubs, :id, :cp, :adresse, :ville, :corres, :mail, :tel, :licensees)";
$fields = [
           'ligue' => $ligue,
           'nom_clubs' => $nom_clubs,
           'id' => $id,
           'cp' => $cp,
           'adresse' => $adresse,
           'ville' => $ville,
           'corres' => $corres,
           'mail' => $mail,
           'tel' => $tel,
           'licensees' => $licensees
         ];

$user = Database::getInstance()->request($sql, $fields, false);
