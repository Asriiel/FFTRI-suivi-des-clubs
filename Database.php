<?php
include_once("connect.php");

/**
 * Classe de gestion de la base de donnée
 */
class Database
{
  // Instance de PDO
  private $_PDOInstance;

  // Instance de Database
  private static $_instance = null;

  // Constructeur de Database avec le pattern singleton
  private function __construct()
  {
    try
    {
      $options =
      [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", // Définit l'encodage en UTF-8
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // Utilise les exception de PDO
        PDO::ATTR_EMULATE_PREPARES => false // Permet de réaliser des requêtes SQL préparées natives
      ];

      $this->_PDOInstance = new PDO('mysql:host='.SERVER.';dbname='.BASE,USER,PASSWD, $options);
    }
    catch (PDOException $e)
    {
      exit($e->getMessage());
    }
  }
  //  Retourne une instance de Database ( existante ou nouvelle)
  /**
  * @return Une instance de base de donnée ~
  */
  public static function getInstance()
  {
    if (is_null(self::$_instance))
      self::$_instance = new Database();
    return self::$_instance;
  }
  /**
  *
  * @param $sql La requête SQL
  * @param $fields Est-ce qu'il y a des champs à traiter ?
  * @param $multiple La requête doit-elle retourner plusieurs résultats ?
  */
  public function request($sql, $fields = false, $multiple = false)
  {
    try
    {
      $statement = $this->_PDOInstance->prepare($sql);
      if ($fields) // S'il existe des champs, on forcera leurs types
      {
        foreach ($fields as $key => $value) {
          if (is_int($value))
            $dataType = PDO::PARAM_INT;
          else if (is_bool($value))
              $dataType = PDO::PARAM_BOOL;
          else if (is_null($value))
              $dataType = PDO::PARAM_NULL;
          else
              $dataType = PDO::PARAM_STR;

          $statement->bindValue(':'.$key, $value, $dataType);
        }
      }
      $statement->execute();

      if ($multiple) // Si plusieurs résultats sont attendus, alors on utilise le fetchAll(PDO::FETCH_OBJ)
        $result = $statement->fetchAll(PDO::FETCH_OBJ); // FETCH_OBJ : $res->title   ~~ FETCH_ASSOC : $res['title']
      else // Sinon on utilise simplement le fetch(PDO::FETCH_OBJ)
        $result = $statement->fetch(PDO::FETCH_OBJ);

      $statement->closeCursor();

      return $result; // on Retourne le résultat dans la variable qui a éxécuté la requête SQL
    }
    catch (Exception $e)
    {
      exit($e->getMessage());
    }

  }
}
