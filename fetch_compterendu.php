<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
?>
<?php
include_once 'Database.php';

$sql = "SELECT * FROM compterendu";
$fields = [];

$compterendu = Database::getInstance()->request($sql, $fields, true);
echo json_encode($compterendu);
