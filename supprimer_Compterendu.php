<?php
include_once 'Database.php';

$id = $_GET['id'];
$sql = "DELETE FROM compterendu WHERE id = :id";
$fields = ['id' => $id];

$user = Database::getInstance()->request($sql, $fields);
$_SESSION['flash']['success'] = "Compte rendu supprimé avec succès !";
