<?php
// Header obligatoire pour les requêtes AJAX
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
?>
<?php
include_once 'Database.php';

$date = $_GET['date'];
$nom_clubs = $_GET['nom_clubs'];
$raison = $_GET['raison'];
$corps = $_GET['corps'];
$contact = $_GET['contact'];
$mail = $_GET['mail'];
$tel = $_GET['tel'];

$sql = "SELECT * FROM compterendu WHERE date = :date";
$fields = ['date' => $date];
$instance = Database::getInstance()->request($sql, $fields);
if ($instance) {
  $sql = "DELETE FROM compterendu WHERE date = :date";
  $fields = ['date' => $date];
  $instance = Database::getInstance()->request($sql, $fields);
}

$sql = "INSERT INTO compterendu (nom_clubs, contact, tel, mail, raison, corps, date) VALUES (:nom_clubs, :contact, :tel, :mail, :raison, :corps, :date)";
$fields = ['nom_clubs' => $nom_clubs, 'contact' => $contact, 'tel' => $tel, 'mail' => $mail, 'raison' => $raison, 'corps' => $corps, 'date' => $date];
$user = Database::getInstance()->request($sql, $fields, false);
