<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>
<?php
if (!empty($_POST)) {

    $contact = $_POST['contact'];
    $mail = $_POST['mail'];
    $tel = $_POST['tel'];
    $club = $_POST['club'];
    $structure = $_POST['structure'];

    $sql = "INSERT INTO contact (nom_clubs, contact, tel, mail, structure) VALUES (:nom_clubs, :contact, :tel, :mail, :structure)";
    $fields = ['nom_clubs' => $club, 'contact' => $contact, 'tel' => $tel, 'mail' => $mail, 'structure' => $structure];
    $user = Database::getInstance()->request($sql, $fields, false);
    $_SESSION['flash']['success'] = "Contact ajouté avec succès !";

  header('Location: listeContact.php');
}
?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Création de contact
                </li>
            </ol>
        </div>
    </div>
  <form action="" method="POST" role="form" class="form-horizontal">
      <fieldset>

      <legend>Créer un contact</legend>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="date"> Nom du contact </label>
        <div class="col-md-4">
        <input id="contact" name="contact" placeholder="placeholder" class="form-control input-md" required="" type="text">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="date"> Structure de rattachement </label>
        <div class="col-md-4">
        <input id="structure" name="structure" placeholder="placeholder" class="form-control input-md" required="" type="text">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="nom_clubs"> Mail </label>
        <div class="col-md-4">
        <input id="mail" name="mail" placeholder="placeholder" class="form-control input-md" required="" type="text">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="corres"> Téléphone </label>
        <div class="col-md-4">
          <input id="tel" name="tel" placeholder="placeholder" class="form-control input-md" required="" type="text">
        </div>
      </div>
      <?php
        $id = $_GET['id'];
        $sql = "SELECT * FROM ligues WHERE id = :id";
        $fields = ['id' => $id];
        $getLigueByID = Database::getInstance()->request($sql, $fields);
      ?>
      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="raison">Nom de la ligue</label>
        <div class="col-md-4">
          <input id="club" name="club" placeholder="placeholder" class="form-control input-md" required="true" type="text" value="<?= $getLigueByID->nom_ligue; ?>">
        </div>
      </div>
      <input id="id_club" name="id_club" type="hidden" name="" value="<?= $_GET['id']; ?>">
      <!-- Button -->
      <div class="form-group">

        <div class="col-md-4 col-md-offset-4">
          <div class="col-sm-6">
            <button class="btn btn-default" type="submit" name="button">Enregistrer</button>
          </div>

        </div>
      </div>

      </fieldset>
    </form>
</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- <script src="js/new_contact.js"></script> -->

</body>
</html>
