<?php

// Gestions des erreurs simplifié
function debug($variable){
  echo '<pre>'. print_r($variable, true) . '</pre>';
}

/*
  Autorise l'accès à une page si et seulement si l'utilisateur est
  connecté sinon il est rédirigé vers la page de connexion
*/
function logged_only(){
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  if (!isset($_SESSION['auth'])){
    $_SESSION['flash']['danger'] = " Vous n'avez pas le droit d'accéder à cette page";
    header('Location: login.php');
    exit();
  }
}

// function getClubsById($id){
//   $sql = "SELECT * FROM clubs WHERE id = :id";
//   $fields = ['id' => $id];
//   $infosClub = Database::getInstance()->request($sql, $fields);
// }
