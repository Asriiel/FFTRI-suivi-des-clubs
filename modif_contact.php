<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>

<?php

if (!empty($_POST)) {

  $contact = $_POST['contact'];
  $mail = $_POST['mail'];
  $tel = $_POST['tel'];
  $club = $_POST['club'];
  $structure = $_POST['structure'];
  $id_contact = $_POST['id_contact'];

  // Requête permettant l'update d'un contact en base
  $sql = "UPDATE contact SET nom_clubs = :nom_clubs, contact = :contact, tel = :tel, mail = :mail, structure = :structure WHERE id = :id";
  $fields = ['nom_clubs' => $club,
             'contact' => $contact,
             'tel' => $tel,
             'mail' => $mail,
             'structure' => $structure,
             'id' => $id_contact];

  $user = Database::getInstance()->request($sql, $fields, false);
  // Message pour informer l'utilisation que le contact à bien été mis à jour dans la variable $_SESSION['flash']
  $_SESSION['flash']['success'] = "Contact mis à jour avec succès !";
  // Redirection vers la page listeContact.php
  header('Location: listeContact.php');
}

?>
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Modifications compte rendu
                </li>
            </ol>
        </div>
    </div>
    <?php
      // Requête pour récupérer le contact possédant l'id passé dans l'URL (method GET)
      $id = $_GET['id'];
      $sql = "SELECT * FROM contact WHERE id = :id";
      $fields = ['id' => $id];
      $infosContact = Database::getInstance()->request($sql, $fields);
    ?>
    <form action="" method="POST" role="form" class="form-horizontal">
        <fieldset>

        <!-- Form Name -->
        <legend>Créer un contact</legend>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="date"> Nom du contact </label>
          <div class="col-md-4">
          <input id="contact" name="contact" placeholder="placeholder" class="form-control input-md" required="true" value="<?= $infosContact->contact; ?>" type="text">

          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="date"> Structure de rattachement </label>
          <div class="col-md-4">
          <input id="structure" name="structure" placeholder="placeholder" class="form-control input-md" required=" true" value="<?= $infosContact->structure; ?>" type="text">

          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="nom_clubs"> Mail </label>
          <div class="col-md-4">
          <input id="mail" name="mail" placeholder="placeholder" class="form-control input-md" required="true" value="<?= $infosContact->mail; ?>" type="text">

          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="corres"> Téléphone </label>
          <div class="col-md-4">
            <input id="tel" name="tel" placeholder="placeholder" class="form-control input-md" required="true" value="<?= $infosContact->tel; ?>" type="text">
          </div>
        </div>

        <!-- Select Basic -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="raison">Nom du club</label>
          <div class="col-md-4">
            <input id="club" name="club" placeholder="placeholder" class="form-control input-md" required="true" value="<?= $infosContact->nom_clubs; ?>" type="text">

          </div>
        </div>
        <input id="id_contact" name="id_contact" type="hidden" name="" value="<?= $_GET['id']; ?>">

        <!-- Button -->
        <div class="form-group">

          <div class="col-md-4 col-md-offset-4">
            <div class="col-sm-6">
              <button class="btn btn-default" type="submit" name="button">Enregistrer</button>
            </div>

          </div>
        </div>

        </fieldset>
      </form>
</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>
