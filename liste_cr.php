<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>


<div class="container-fluid">
  <!-- Méthode permettant l'affichage des messages dans $_SESSION['flash'] -->
  <?php if (isset($_SESSION['flash'])): ?>
    <?php foreach ($_SESSION['flash'] as $type => $message): ?>
      <div class="alert alert-<?= $type; ?>">
        <center><?= $message; ?></center>
      </div>
    <?php endforeach; ?>
    <?php unset($_SESSION['flash']); ?>
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Liste des comptes rendus
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <h3 style="text-align:center;">Liste des comptes rendus </h3>
        <div id="listecr" name="listecr" class="jumbotron">
          <?php
            // Requête pour récuperer la totalité des comptes rendus
            $sql = "SELECT * FROM compterendu";
            $fields = [];
            $listeCr = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeCr as $cr): ?>
            <div class="card">
              <h3 class="card-header"><?= $cr->nom_clubs; ?><small> <?= $cr->date; ?> </small></h3>
                <div class="card-block">
                  <h4 class="card-title"><?= $cr->raison; ?></h4>
                  <p style="font-size: 16px;" class="card-text"><?= $cr->corps; ?></p>
                  <a href="modif_cr.php?id=<?= $cr->id; ?>&club=<?= $cr->nom_clubs; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a href="#" onclick="supprimerCompterendu(<?= $cr->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                  <!-- Tester avec la fonction mail de php -->
                  <!-- <a href="#" onclick="sendMail()" class="btn btn-info">Envoyer mail</a> -->
                </div>
            </div>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/liste_cr.js"></script>

</body>
</html>
