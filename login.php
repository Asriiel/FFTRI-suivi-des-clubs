<?php session_start(); ?>
<?php require_once 'function.php'; ?>
<?php require 'header.php'; ?>

<?php
  if (!empty($_POST)) {

    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $sql = "SELECT * FROM users WHERE email = :email AND passwd = :passwd";
    $fields = ['email' => $email, 'passwd' => $password];
    $user = Database::getInstance()->request($sql, $fields, false);
    if (!empty($user)) {
      $_SESSION['auth'] = $user;
      $_SESSION['flash']['success'] = "Vous êtes maintenant connecté !";
      header('Location: index.php');
    } else {
      $_SESSION['flash']['danger'] = "Vous avez renseigné un mauvais Mot de passe ou un mauvais Login !";
      header('Location: login.php');
    }

  }
?>

<br><br><br><br>
<div class="col-md-10 col-md-offset-1">
  <?php if (isset($_SESSION['flash'])): ?>
    <?php foreach ($_SESSION['flash'] as $type => $message): ?>
      <div class="alert alert-<?= $type; ?>">
        <?= $message; ?>
        <?php unset($_SESSION['flash'][$type]) ?>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>


    <div class="modal-dialog" style="margin-bottom:0">
        <div class="modal-content">
                    <div class="panel-heading">
                        <h3 class="panel-title">Se connecter</h3>
                    </div>
                    <div class="panel-body">
                        <form action="" method="POST" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                                </div>

                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-primary">Se connecter</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
    </div>
</div>
