<?php
  include_once 'Database.php';
  require_once 'function.php';
  logged_only();

  if (!empty($_POST)) {
    if (empty($_POST['passwd']) || $_POST['passwd'] != $_POST['password_confirm']) {
      $_SESSION['flash']['danger'] = " Les mots de passes ne correspondent pas";
    } else {
      $user_id = $_SESSION['auth']->id;
      $password = md5($_POST['passwd']);
      $sql = "UPDATE users SET passwd = :passwd WHERE id = :id";
      $fields = ['passwd' => $password, 'id' => $user_id];
      $_SESSION['flash']['success'] = " Votre mot de passe à été mis à jour avec succès";
      $updateMdp = Database::getInstance()->request($sql, $fields);

    }
  }
?>
<!-- Header -->
<?php
  include 'header.php';
?>


<div id="page-wrapper">

  <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          F.F.TRI <small>Gestion des clubs</small>
        </h1>
        <ol class="breadcrumb">
          <li class="active">
            <i class="fa fa-dashboard"></i> F.F.TRI
          </li>
          <li class="active">
            <i class="fa fa-edit"></i> Administration
          </li>

        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <?php if (isset($_SESSION['flash'])): ?>
          <?php foreach ($_SESSION['flash'] as $type => $message): ?>
            <div class="alert alert-<?= $type; ?>">
              <?= $message; ?>
            </div>
          <?php endforeach; ?>
          <?php unset($_SESSION['flash']); ?>

        <?php endif; ?>
        <h3 style="text-align:center;">Administration de la base de données </h3>

        <div class="col-md-10 col-md-offset-1">
          <div class="modal-dialog" style="margin-bottom:0">
            <div class="modal-content">
              <div class="panel-heading">
                <center><h2 class="panel-title">Synchronisation</h2></center>
              </div>
              <div class="panel-body">
                <center>
                  <div class="row">
                    <div class="col-sm-6">
                      <a href="#" id="AjaxClubs" class="btn btn-success">Synchronisation Clubs</a>
                    </div>
                    <div class="col-sm-6">
                      <a href="#" id="AjaxLigues" class="btn btn-success">Synchronisation Ligues</a>
                    </div>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
          <div class="modal-dialog" style="margin-bottom:0">
            <div class="modal-content">
              <div class="panel-heading">
                <center><h3 class="panel-title">Créer un nouvel utilisateur</h3></center>
              </div>
              <div class="panel-body">
                <form action="insert_user.php" method="POST" role="form">
                  <fieldset>
                    <div class="form-group">
                      <input class="form-control" placeholder="Email ou nom d'utilisateur" name="username" type="text" required="true" value="">
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Mot de passe" name="new_passwd" type="password" required="true" value="">
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Confirmation" name="new_password_confirm" type="password" required="true">
                    </div>
                    <center> <button type="submit" class="btn btn-primary">Enregistrer</button> </center>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>

      <div class="col-md-10 col-md-offset-1">
        <div class="modal-dialog" style="margin-bottom:0">
          <div class="modal-content">
            <div class="panel-heading">
              <center><h3 class="panel-title">Changer le mot de passe</h3></center>
            </div>
            <div class="panel-body">
              <form action="" method="POST" role="form">
                <fieldset>
                  <div class="form-group">
                    <input class="form-control" placeholder="Nouveau mot de passe" name="passwd" type="password" required="true">
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Confirmation" name="password_confirm" type="password" required="true">
                  </div>
                  <center> <button type="submit" class="btn btn-primary">Enregistrer</button> </center>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>

    </div>
  </div>
</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin.js"></script>

</body>
</html>
