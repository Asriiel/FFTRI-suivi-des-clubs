<?php
  session_start();
  require_once 'function.php';
  logged_only();
?>
<!-- Header -->
<?php include 'header.php'; ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                F.F.TRI <small>Gestion des clubs</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> F.F.TRI
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Ligues
                </li>
                <li id="ligue_active" class="active">
                  <?php
                    $id = $_GET['id'];
                    $sql = "SELECT * FROM ligues WHERE id = :id";
                    $fields = ['id' => $id];
                    $infosLigue = Database::getInstance()->request($sql, $fields);
                    echo $infosLigue->nom_ligue;
                  ?>
                </li>
            </ol>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-5 col-md-5">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-send fa-5x"></i>
                        </div>
                        <div id="status" name="status" class="col-xs-9">
                          <?php
                            // Requête permettant de récupérer les informations d'une ligue grâce à son id passé dans l'URL (method GET)
                            $id = $_GET['id'];
                            $sql = "SELECT * FROM ligues WHERE id = :id";
                            $fields = ['id' => $id];
                            $infosLigue = Database::getInstance()->request($sql, $fields);
                          ?>
                          <div><strong>Adresse : </strong><?= $infosLigue->adresse; ?></div>
                          <div><strong>Ville : </strong><?= $infosLigue->ville; ?></div>
                          <div><strong>Code postal : </strong><?= $infosLigue->cp; ?></div>
                          <div><strong>Téléphone : </strong><?= $infosLigue->tel; ?></div>
                          <div><strong>Email : </strong><?= $infosLigue->mail; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="clubsAffiliés" name="clubsAffiliés" class="huge">
                              <?php
                                // Requête permettant de récupérer le nombre de clubs affilié à une ligue
                                $id = $_GET['id'];
                                $sql = "SELECT COUNT(*) AS nb FROM clubs c, ligues l WHERE l.id = :id AND l.nom_ligue=c.ligue";
                                $fields = ['id' => $id];
                                $countClubs = Database::getInstance()->request($sql, $fields);
                                echo $countClubs->nb;
                              ?>
                            </div>
                            <div>Clubs affiliés</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="licensees" name="licensees" class="huge">
                              <?php
                                // Requête pour récupérer le nombre de licenciés d'une ligue
                                $id = $_GET['id'];
                                $sql = "SELECT * FROM ligues WHERE id = :id";
                                $fields = ['id' => $id];
                                $licencees = Database::getInstance()->request($sql, $fields);
                                echo $licencees->licensees;
                              ?>
                            </div>
                            <div>Licenciés</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="ajoutcontact">
      <center><a class="btn btn-primary" href="new_contact_ligue.php?id=<?= $infosLigue->id; ?>">Créer un contact pour cette ligue</a></center>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h3 style="text-align:center;"> Contacts de la ligue </h3>
        <div id="listecontact" name="listecontact" class="jumbotron">
          <?php
            // Requête permettant de récupérer la liste des contact pour un club dont l'id est passé dans l'URL (method GET)
            $id = $_GET['id'];
            $sql = "SELECT co.* FROM contact co, ligues l WHERE l.id = :id AND co.nom_clubs=l.nom_ligue";
            $fields = ['id' => $id];
            $listeContact = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeContact as $contact): ?>
            <center>
              <div class="card">
                <h3 class="card-header"><?= $contact->contact; ?></h3>
                <div class="card-block">
                  <p class="card-text"><?= $contact->mail; ?></p>
                  <p class="card-text"><?= $contact->tel; ?> </p>
                    <a href="modif_contact.php?id=<?= $contact->id; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a onclick="supprimerContact(<?= $contact->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                </div>
              </div>
            </center>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <br>
    <div id="compterendu">
      <center><a class="btn btn-primary" href="new_cr_ligue.php?id=<?= $infosLigue->id; ?>">Créer un compte rendu pour cette ligue</a></center>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h3 style="text-align:center;"> Comptes rendus de la ligue </h3>
        <div id="listecr" name="listecr" class="jumbotron">
          <?php
            // Requête permettant de récupéter les comptes rendus pour un club
            $id = $_GET['id'];
            $sql = "SELECT co.* FROM compterendu co, ligues l WHERE l.id = :id AND co.nom_clubs=l.nom_ligue";
            $fields = ['id' => $id];
            $listeCr = Database::getInstance()->request($sql, $fields, true);
          ?>
          <?php foreach ($listeCr as $cr): ?>
            <div class="card">
              <h3 class="card-header"><?= $cr->nom_clubs; ?><small> <?= $cr->date; ?> </small></h3>
                <div class="card-block">
                  <h4 class="card-title"><?= $cr->raison; ?></h4>
                  <p style="font-size: 16px;" class="card-text"><?= $cr->corps; ?></p>
                  <a href="modif_cr.php?id=<?= $cr->id; ?>&club=<?= $cr->nom_clubs; ?>" id="modifier" name="modifier" class="btn btn-primary">Modifier</a>
                  <a onclick="supprimerCompterendu(<?= $cr->id; ?>)" id="supprimer" name="supprimer" class="btn btn-danger">Supprimer</a>
                  <!-- Tester avec la fonction mail de php  -->
                  <!-- <a href="mailto:?subject=Compte Rendu du club : '+ nom_clubs_ +'&body=Date: Le '+ date_ +', %0D%0A%0D%0A Correspondant: '+corres_+'%0D%0A Mail: '+mail_+'%0D%0A Tel: 0'+tel_+' %0D%0A%0D%0A Motif: '+raison_+' %0D%0A%0D%0A Compte rendu: '+corps_+' " class="btn btn-info">Envoyer mail</a> -->
                </div>
            </div>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <center><a class="btn btn-info" href="https://drive.google.com/drive/u/2/folders/0B3Y_kdvi3603bUk3RFpzYkl1SVk">Drive</a></center>
    <br><br><br>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" id="listeClubs2" name="listeClubs2"><i class="fa fa-globe fa-fw"></i> Liste des clubs de la ligue : <?= $infosLigue->nom_ligue; ?></h3>
                </div>
                <div class="panel-body">
                  <div class="list-group">
                    <div id="listeClubs" name="listeClubs">
                      <?php
                        // Requête pour récupérer la liste des clubs affilié à une ligue
                        $id = $_GET['id'];
                        $sql = "SELECT c.id, c.nom_clubs FROM clubs c, ligues l WHERE l.id = :id AND l.nom_ligue=c.ligue ORDER BY nom_clubs ASC";
                        $fields = ['id' => $id];
                        $listeClubs = Database::getInstance()->request($sql, $fields, true);
                      ?>
                      <!-- Dans une boucle on affiche les liens vers les clubs -->
                      <?php foreach ($listeClubs as $club): ?>
                        <a href="club.php?id=<?= $club->id; ?>" class="list-group-item"><i class="fa fa-fw fa-users"></i> <?= $club->nom_clubs; ?></a>
                      <?php endforeach; ?>
                    </div>
                      <ul class="pagination" id="pagin">
                      </ul>
                        <br>
                        <div class="col-sm-3">
                          <label class="sr-only">Nom</label>
                          <input type="text" onkeyup="recherche(event)" id="nametxt" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Rechercher">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/liste_cr.js"></script>
<script src="js/liste_contact.js"></script>

</body>
</html>
